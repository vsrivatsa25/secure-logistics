from django.db import models
from django.contrib.auth.models import User


class Activation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    code = models.CharField(max_length=20, unique=True)
    email = models.EmailField(blank=True)
    totp_id = models.CharField(max_length=50, unique=True)

class Order(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    type = models.CharField(max_length=20, null=False)
    from_addr = models.CharField(max_length=50)
    to_addr = models.CharField(max_length=50)
    recipient = models.ForeignKey(User, on_delete=models.CASCADE, related_name='recipient')
    description = models.CharField(max_length=100)
    payment_method = models.CharField(max_length=30)
    payment_status = models.CharField(max_length=30)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    status = models.CharField(max_length=30, null=False)


class OrderTracking(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    customer = models.ForeignKey(User, on_delete=models.CASCADE)
    placed_at = models.DateTimeField(null=False)
    accept_at = models.DateTimeField(null=True)
    dispatched_at = models.DateTimeField(null=True)
    delivery_date = models.DateTimeField(null=True)
    delivered_date = models.DateTimeField(null=True)
    order_operator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='operator')
    otp_verified = models.BooleanField(default=False, null=False)
    current_status = models.CharField(max_length=30, null=False)