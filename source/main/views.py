from django.views.generic import TemplateView


class IndexPageView(TemplateView):
    template_name = 'main/index.html'

class TOTPPageView(TemplateView):
    template_name = 'main/2fa.html'

class ChangeLanguageView(TemplateView):
    template_name = 'main/change_language.html'
