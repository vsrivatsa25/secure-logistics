# Secure E-Logistics

A secure E-commerce website that makes use of Two-Factor Authentication, Access Control and Session
management to create and demonstrate a website and database for secure operations management.


## Installing

### Clone the project

```
git clone https://bitbucket.org/vsrivatsa25/secure-logistics
cd secure-logistics
```

### Install dependencies & activate virtualenv

```
pip install pipenv

pipenv install
pipenv shell
```

### Configure the settings (connection to the database, connection to an SMTP server, and other options)

1. Edit `source/app/conf/development/settings.py` if you want to develop the project.

2. Edit `source/app/conf/production/settings.py` if you want to run the project in production.

### Apply migrations

```
python source/manage.py migrate
```

### Collect static files (only on a production server)

```
python source/manage.py collectstatic
```

### Running

#### A development server

Just run this command:

```
python source/manage.py runserver
```
